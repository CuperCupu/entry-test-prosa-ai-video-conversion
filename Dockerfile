FROM python:3.8.5-alpine

RUN apk add ffmpeg=4.3.1-r0

WORKDIR /build/
ADD requirements.txt requirements.txt
RUN pip install -r requirements.txt
RUN pip install gunicorn==20.0.4

WORKDIR /
RUN rm -r build/

WORKDIR /service/

ADD src src
ADD presets presets

ENV WORKERS 4

CMD gunicorn --workers=$WORKERS  --timeout 0 --pythonpath src -b 0.0.0.0:80 web_server:app