# Video Conversion Service

## Tests

To run the tests using docker-compose
```bash
$ docker-compose -f docker-compose.test.yml build
$ docker-compose -f docker-compose.test.yml run test
```
