import sys

from video.conversion import convert_file
from video import presets


def main():
    available_presets = presets.load_dir('presets')

    filename = sys.argv[1]
    out_filename = sys.argv[2]
    preset_name = sys.argv[3] if len(sys.argv) > 3 else None

    if preset_name in presets:
        using_preset = available_presets[preset_name]
        out_filename = using_preset.resolve_filename(out_filename)
        options = using_preset.options_list
        convert_file(filename, out_filename, options)
    else:
        convert_file(filename, out_filename)


if __name__ == '__main__':
    main()
