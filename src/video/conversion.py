from subprocess import PIPE, Popen


def is_ffmpeg_installed():
    try:
        _, _, returncode = execute_commands(["ffmpeg", "-version"])
        return returncode == 0
    except FileNotFoundError:
        return False


def execute_commands(commands):
    proc = Popen(commands, stdout=PIPE, stderr=PIPE)
    result, err = proc.communicate()
    return result, err, proc.returncode


def construct_convert_commands(source_filename, destination_filename, options=()):
    return ['ffmpeg', '-i', source_filename, *options, '-y', '-max_muxing_queue_size', '1024', destination_filename]


def check_file(filename):
    commands = ["ffprobe", "-i", filename, "-hide_banner"]
    _, _, rc = execute_commands(commands)
    return rc == 0


def convert_file(source_filename, destination_filename, options=()):
    commands = construct_convert_commands(source_filename, destination_filename, options)
    return execute_commands(commands)
