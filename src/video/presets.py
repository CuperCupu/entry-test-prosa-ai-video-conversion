import json
import os
from typing import Dict, List


class Preset:

    def __init__(self, extension: str, options: dict):
        self.extension = extension
        self.options = options

    def resolve_filename(self, filename) -> str:
        return '.'.join(filename.split('.')[:-1]) + '.' + self.extension

    @property
    def options_string(self) -> str:
        return ' '.join(f"-{key} {value}" for key, value in self.options.items())

    @property
    def options_list(self) -> List[str]:
        options = []
        for key, value in self.options.items():
            options.append('-' + key)
            options.append(value)
        return options

    def __repr__(self):
        return f"<preset {self.extension} '{self.options_string}'>"


def from_dict(data) -> Preset:
    return Preset(data.get('extension'), data.get('options'))


def from_json_file(filename) -> Preset:
    with open(filename) as f:
        return from_dict(json.load(f))


def load_dir(dirname) -> Dict[str, Preset]:
    presets = {}
    for name in os.listdir(dirname):
        filename = os.path.join(dirname, name)
        presets['.'.join(name.split('.')[:-1])] = from_json_file(filename)
    return presets
