import os
import random
import string
import sys
import time
from io import BytesIO
from logging import basicConfig, getLogger, INFO

from flask import Flask, request, abort, send_file

from video import presets
from video.conversion import convert_file, is_ffmpeg_installed


basicConfig(format='%(asctime)s: %(levelname)s - %(name)s: %(message)s')

logger = getLogger('converter')
logger.setLevel(INFO)

if not is_ffmpeg_installed():
    logger.error("ffmpeg not found, aborting")
    sys.exit()

app = Flask(__name__)


class ProcessingContext:

    def __init__(self, tmp_dir='./tmp'):
        self.tmp_dir = tmp_dir
        self.abs_tmp_dir = os.path.abspath(tmp_dir)
        self._files_to_clean_up = [
        ]

    def abs(self, filename):
        return os.path.join(self.abs_tmp_dir, filename)

    def new_file(self, filename):
        filename = self.abs(''.join(random.choices(string.ascii_letters, k=20)) + '_' + filename)
        self._files_to_clean_up.append(filename)

        return filename

    def __enter__(self):
        os.makedirs(self.abs_tmp_dir, exist_ok=True)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        for filename in self._files_to_clean_up:
            try:
                os.unlink(filename)
            except FileNotFoundError as e:
                logger.error("Error when cleaning up processing context", exc_info=e)


available_presets = presets.load_dir('presets')

logger.info("Available presets:\n" +'\n'.join(f"    {key}: {value}" for key, value in available_presets.items()))


@app.route("/", methods=['POST'])
def convert_video():
    if 'preset' not in request.form:
        abort(400, "missing preset parameter")
    preset_name = request.form['preset']
    if preset_name not in available_presets:
        abort(400, f"unknown preset: {preset_name}")
    using_preset = available_presets[preset_name]
    logger.info(f"Converting with preset {preset_name}: {using_preset}")
    for name, file in request.files.items():
        with ProcessingContext() as c:
            input_filename = c.new_file(file.filename)
            file.save(input_filename)

            output_filename = c.new_file(using_preset.resolve_filename(file.filename))

            options = using_preset.options_list

            start_time = time.perf_counter()
            convert_file(input_filename, output_filename, options)
            end_time = time.perf_counter()
            logger.info(f"Converted with preset {preset_name}, elapsed: {end_time - start_time}")

            with open(output_filename, 'br') as f:
                return send_file(BytesIO(f.read()), attachment_filename=output_filename)
    abort(400, "no file to process")


if __name__ == '__main__':
    app.run()
