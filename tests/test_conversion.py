import hashlib
import os

from video.conversion import convert_file, check_file

TEST_INPUT_VIDEO = 'tests/test.mp4'
TEMP_OUTPUT = 'output.mov'
INPUT_HASH = 'd0ab9861310eaffc4a1da74a89ad2cc4'


def calculate_digest(f):
    file_hash = hashlib.md5()
    while chunk := f.read(8192):
        file_hash.update(chunk)
    return file_hash.hexdigest()


def test_convert():
    with open(TEST_INPUT_VIDEO, 'rb') as f:
        assert calculate_digest(f) == INPUT_HASH

    res, err, rc = convert_file(
        TEST_INPUT_VIDEO,
        TEMP_OUTPUT,
        ['-b:a', '32k', '-crf', '28', '-vcodec', 'libx264', '-acodec', 'aac']
    )

    print(err, rc)

    assert rc == 0

    assert os.path.isfile(TEMP_OUTPUT)

    assert check_file(TEMP_OUTPUT)

    os.unlink(TEMP_OUTPUT)
