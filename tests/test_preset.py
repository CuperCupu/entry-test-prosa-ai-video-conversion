import json

from video import presets


def test_preset_from_dict():
    preset = presets.from_dict({
        "extension": "mp4",
        "options": {
            "b": "32k",
            "crf": "23"
        }
    })
    assert preset.extension == "mp4"
    assert preset.options_list == ["-b", "32k", "-crf", "23"]


def test_preset_from_json_file(tmpdir):
    json_file = tmpdir.join("preset.json")
    with open(json_file, 'w') as f:
        json.dump({
            "extension": "mp4",
            "options": {
                "b": "32k",
                "crf": "23"
            }
        }, f)
    preset = presets.from_json_file(json_file)
    assert preset.extension == "mp4"
    assert preset.options_list == ["-b", "32k", "-crf", "23"]
