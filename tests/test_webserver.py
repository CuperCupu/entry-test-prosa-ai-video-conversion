import hashlib
import os
from io import BytesIO

import pytest

import web_server
from video.conversion import check_file

TEST_INPUT_VIDEO = 'tests/test.mp4'
INPUT_HASH = 'd0ab9861310eaffc4a1da74a89ad2cc4'
TEMP_OUTPUT = 'output.mov'


def calculate_digest(f):
    file_hash = hashlib.md5()
    while chunk := f.read(8192):
        file_hash.update(chunk)
    return file_hash.hexdigest()


@pytest.fixture()
def client():
    yield web_server.app.test_client()


def test_convert_file(client):
    with open(TEST_INPUT_VIDEO, 'br') as f:
        assert calculate_digest(f) == INPUT_HASH
        f.seek(0)
        file_content = BytesIO(f.read())

    response = client.post(
        "/",
        content_type='multipart/form-data',
        data={
            'preset': 'mov-low',
            'file': (file_content, os.path.basename(TEST_INPUT_VIDEO))
        }
    )

    assert response.status_code == 200

    with open(TEMP_OUTPUT, 'bw') as f:
        f.write(response.data)

    assert check_file(TEMP_OUTPUT)

    os.unlink(TEMP_OUTPUT)
